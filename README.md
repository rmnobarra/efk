Manifestos para stack efk

1. Clone do repo:
```bash
git clone https://gitlab.com/rmnobarra/efk.git
```
2. Aplicando manifestos:
```bash
cd efk && ./deploy.sh
```
3. Adicionando dados para teste (opcional):
```bash
kubectl apply -f counter.yaml -n kube-logging
```
---
## Checks

### ES:
kubectl rollout status sts/es-cluster --namespace=kube-logging
kubectl port-forward es-cluster-0 9200:9200 --namespace=kube-logging
curl http://localhost:9200/_cluster/state?pretty

### Kibana:
kubectl rollout status deployment/kibana --namespace=kube-logging
kubectl port-forward kibana-6c9fb4b5b7-plbg2 5601:5601 --namespace=kube-logging

Endpoint: http://localhost:5601

[referência](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-elasticsearch-fluentd-and-kibana-efk-logging-stack-on-kubernetes-pt)