echo "Criando namespace..."
kubectl apply -f kube-logging.yaml
echo "Criando service para elasticsearch..."
kubectl apply -f elasticsearch_svc.yaml
echo "Criando statefulset para elasticseach..."
kubectl apply -f elasticsearch_statefulset.yaml
sleep 90
echo "Configurando kibana..."
kubectl apply -f kibana.yaml
echo "Configurando fluentd..."
kubectl apply -f fluentd.yaml
echo "Criando ingresses..."
kubectl apply -f ingress.yaml
echo "Pronto =)"
